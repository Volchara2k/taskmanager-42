package ru.renessans.jvschool.volkov.task.manager.api.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

public interface IIndexController {

    @NotNull
    @GetMapping("/")
    ModelAndView index();

}