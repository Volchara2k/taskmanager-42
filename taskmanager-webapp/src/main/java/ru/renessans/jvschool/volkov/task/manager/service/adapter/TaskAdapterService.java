package ru.renessans.jvschool.volkov.task.manager.service.adapter;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.ITaskAdapterService;
import ru.renessans.jvschool.volkov.task.manager.dto.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.TimeFrameDTO;
import ru.renessans.jvschool.volkov.task.manager.model.Project;
import ru.renessans.jvschool.volkov.task.manager.model.Task;
import ru.renessans.jvschool.volkov.task.manager.model.TimeFrame;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public final class TaskAdapterService implements ITaskAdapterService {

    @Nullable
    @Override
    public TaskDTO toDTO(@Nullable final Task convertible) {
        if (Objects.isNull(convertible)) return null;
        return TaskDTO.builder()
                .id(convertible.getId())
                .userId(convertible.getUserId())
                .projectId(!StringUtils.isEmpty(convertible.getProject()) ? convertible.getProject().getId() : null)
                .title(convertible.getTitle())
                .description(convertible.getDescription())
                .timeFrame(
                        TimeFrameDTO.builder()
                                .creationDate(convertible.getTimeFrame().getCreationDate())
                                .startDate(convertible.getTimeFrame().getStartDate())
                                .endDate(convertible.getTimeFrame().getEndDate())
                                .build()
                )
                .status(convertible.getStatus())
                .build();
    }

    @Nullable
    @Override
    public Task toModel(@Nullable final TaskDTO convertible) {
        if (Objects.isNull(convertible)) return null;
        return Task.builder()
                .id(
                        !StringUtils.isEmpty(convertible.getId())
                                ? convertible.getId() : UUID.randomUUID().toString()
                )
                .userId(convertible.getUserId())
                .project(
                        (!StringUtils.isEmpty(convertible.getProjectId()))
                                ? new Project(convertible.getProjectId()) : null
                )
                .title(convertible.getTitle())
                .description(convertible.getDescription())
                .timeFrame(
                        TimeFrame.builder()
                                .creationDate(!Objects.isNull(convertible.getTimeFrame().getCreationDate())
                                        ? convertible.getTimeFrame().getCreationDate() : new Date())
                                .startDate(convertible.getTimeFrame().getStartDate())
                                .endDate(convertible.getTimeFrame().getEndDate())
                                .build()
                )
                .status(convertible.getStatus())

                .build();
    }

}