package ru.renessans.jvschool.volkov.task.manager.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.annotation.XmlType;

@Data
@XmlType
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(description = "All details about the task")
public final class TaskDTO extends AbstractUserOwnerDTO {

    private static final long serialVersionUID = 1L;

    @Nullable
    @ApiModelProperty(
            value = "Unique ID of project",
            example = "7ca5a0e6-5f22-4955-b134-42f826a2d8dc",
            name = "projectId"
    )
    private String projectId;

}