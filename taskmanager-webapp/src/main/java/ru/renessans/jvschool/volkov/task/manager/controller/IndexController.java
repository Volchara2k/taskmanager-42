package ru.renessans.jvschool.volkov.task.manager.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.renessans.jvschool.volkov.task.manager.api.controller.IIndexController;

@Controller
public class IndexController implements IIndexController {

    @NotNull
    @GetMapping("/")
    public ModelAndView index() {
        return new ModelAndView("index");
    }

}