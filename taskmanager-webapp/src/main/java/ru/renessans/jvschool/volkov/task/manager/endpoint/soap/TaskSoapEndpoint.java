package ru.renessans.jvschool.volkov.task.manager.endpoint.soap;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.soap.ITaskSoapEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.service.ITaskUserService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.ITaskAdapterService;
import ru.renessans.jvschool.volkov.task.manager.dto.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.model.Task;
import ru.renessans.jvschool.volkov.task.manager.util.CurrentUserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;

@Controller
@WebService
@RequiredArgsConstructor
public final class TaskSoapEndpoint implements ITaskSoapEndpoint {

    @NotNull
    private final ITaskUserService taskUserService;

    @NotNull
    private final ITaskAdapterService taskAdapterService;

    @WebMethod
    @WebResult(name = "taskDTO", partName = "taskDTO")
    @Nullable
    @Override
    public TaskDTO addTask(
            @WebParam(name = "taskDTO", partName = "taskDTO") @NotNull final TaskDTO taskDTO
    ) {
        @Nullable final Task request = this.taskAdapterService.toModel(taskDTO);
        if (Objects.isNull(request)) return null;
        @NotNull final String userid = CurrentUserUtil.getUserId();
        request.setUserId(userid);
        @NotNull final Task response = this.taskUserService.addOwnerUser(request);
        return this.taskAdapterService.toDTO(response);
    }

    @WebMethod
    @WebResult(name = "taskDTO", partName = "taskDTO")
    @Nullable
    @Override
    public TaskDTO updateTask(
            @WebParam(name = "taskDTO", partName = "taskDTO") @NotNull final TaskDTO taskDTO
    ) {
        @Nullable final Task request = this.taskAdapterService.toModel(taskDTO);
        if (Objects.isNull(request)) return null;
        @NotNull final String userid = CurrentUserUtil.getUserId();
        if (StringUtils.isEmpty(request.getUserId())) request.setUserId(userid);
        @NotNull final Task response = this.taskUserService.addOwnerUser(request);
        return this.taskAdapterService.toDTO(response);
    }

    @WebMethod
    @WebResult(name = "deleteFlag", partName = "deleteFlag")
    @Override
    public int deleteTaskById(
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        @NotNull final String userid = CurrentUserUtil.getUserId();
        return this.taskUserService.deleteOwnerUserById(userid, id);
    }

    @WebMethod
    @WebResult(name = "taskDTO", partName = "taskDTO")
    @Nullable
    @Override
    public TaskDTO getTaskById(
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        @NotNull final String userid = CurrentUserUtil.getUserId();
        @Nullable final Task task = taskUserService.getOwnerUserById(userid, id);
        return this.taskAdapterService.toDTO(task);
    }

    @WebMethod
    @WebResult(name = "tasksDTO", partName = "tasksDTO")
    @NotNull
    @Override
    public Collection<TaskDTO> getAllTasks() {
        @NotNull final String userid = CurrentUserUtil.getUserId();
        return this.taskUserService.getOwnerUserAll(userid)
                .stream()
                .map(this.taskAdapterService::toDTO)
                .collect(Collectors.toList());
    }

}