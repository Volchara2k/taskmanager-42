package ru.renessans.jvschool.volkov.task.manager.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableWebMvc
@Configuration
@ComponentScan(basePackages = "ru.renessans.jvschool.volkov.task.manager.endpoint.rest.api.v1")
public class WebRestConfiguration extends WebMvcConfiguration {

    @Override
    public void configureContentNegotiation(
            @NotNull final ContentNegotiationConfigurer contentNegotiationConfigurer
    ) {
        contentNegotiationConfigurer.defaultContentType(MediaType.APPLICATION_JSON);
    }

}