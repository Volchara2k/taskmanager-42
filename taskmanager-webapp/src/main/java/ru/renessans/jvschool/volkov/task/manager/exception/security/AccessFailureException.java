package ru.renessans.jvschool.volkov.task.manager.exception.security;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class AccessFailureException extends AbstractException {

    @NotNull
    private static final String ACCESS_FAILURE = "Ошибка доступа! Операция недоступна!";

    @NotNull
    private static final String ACCESS_FAILURE_FORMAT = "Ошибка доступа! Возможные причины: %s\n";

    public AccessFailureException() {
        super(ACCESS_FAILURE);
    }

    public AccessFailureException(@NotNull final String message) {
        super(String.format(ACCESS_FAILURE_FORMAT, message));
    }

}