package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import ru.renessans.jvschool.volkov.task.manager.api.service.ITaskUserService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidDescriptionException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidTitleException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidUserIdException;
import ru.renessans.jvschool.volkov.task.manager.model.Task;
import ru.renessans.jvschool.volkov.task.manager.repository.ITaskUserRepository;

@Service
@Transactional
public final class TaskUserService extends AbstractUserOwnerService<Task> implements ITaskUserService {

    public TaskUserService(
            @NotNull final ITaskUserRepository taskUserRepository,
            @NotNull final IUserService userService
    ) {
        super(taskUserRepository, userService);
    }

    @NotNull
    @SneakyThrows
    @Override
    public Task addOwnerUser(
            @Nullable final String userId,
            @Nullable final String title,
            @Nullable final String description
    ) {
        if (StringUtils.isEmpty(userId)) throw new InvalidUserIdException();
        if (StringUtils.isEmpty(title)) throw new InvalidTitleException();
        if (StringUtils.isEmpty(description)) throw new InvalidDescriptionException();
        @NotNull final Task task = new Task(userId, title, description);
        return super.addOwnerUser(task);
    }

}