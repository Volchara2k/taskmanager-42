<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html>

    <head>
        <title>Task Manager</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/styles.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    </head>

    <body>
        <div class="container-content">
            <table class="list-table" height="100%" >
                <header>
                    <tr>
                        <ul>
                            <li><a href="${pageContext.request.contextPath}/">Task Manager</a></li>
                            <sec:authorize access="isAuthenticated()">
                                <li><a href="${pageContext.request.contextPath}/tasks/">Tasks</a></li>
                                <li><a href="${pageContext.request.contextPath}/projects/">Projects</a></li>
                                <li><a href="${pageContext.request.contextPath}/logout">Log out</a></li>
                            </sec:authorize>
                            <sec:authorize access="!isAuthenticated()">
                                <li><a href="${pageContext.request.contextPath}/login/">Log in</a></li>
                            </sec:authorize>
                        </ul>
                    </tr>
                </header>
                <main>
                    <tr>
                        <td height="100%" valign="top" style="padding: 5%;">