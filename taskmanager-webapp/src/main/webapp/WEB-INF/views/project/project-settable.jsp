<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<jsp:include page="../../include/_header.jsp" />

<c:choose>
    <c:when test="${not empty project.title}">
         <div class="container">
            <div class="project_activity-content">
                <h2>Edit project: &laquo;${project.title}&raquo;</h2>
            </div>

            <form:form action="/project/edit/${project.id}" method="POST" modelAttribute="project">
                <jsp:include page="_project-input-form.jsp" />
            </form:form>
         </div>
    </c:when>

    <c:otherwise>
        <div class="container">
            <div class="task_activity-content">
                <h2>Create project</h2>
            </div>

            <form:form action="/project/create/" method="POST" modelAttribute="project">
                <jsp:include page="_project-input-form.jsp" />
            </form:form>
        </div>
    </c:otherwise>

</c:choose>

<jsp:include page="../../include/_footer.jsp" />``