package ru.renessans.jvschool.volkov.task.manager.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;

@Entity
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@Table(name = "tm_session")
public final class Session extends AbstractModel implements Cloneable {

    @Nullable
    @Column(nullable = false)
    private Long timestamp = 0L;

    @Nullable
    @Column(nullable = false)
    private String userId;

    @Nullable
    @Column(nullable = false)
    private String signature = "";

    @Nullable
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    public Session(
            @NotNull final String userId,
            @NotNull final Long timestamp
    ) {
        setUserId(userId);
        setTimestamp(timestamp);
    }

    @Nullable
    @Override
    public Session clone() {
        try {
            return (Session) super.clone();
        } catch (@NotNull final CloneNotSupportedException exception) {
            return null;
        }
    }

}