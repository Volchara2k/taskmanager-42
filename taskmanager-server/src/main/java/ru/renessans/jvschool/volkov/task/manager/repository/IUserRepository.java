package ru.renessans.jvschool.volkov.task.manager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Query;
import ru.renessans.jvschool.volkov.task.manager.model.User;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    @Query("FROM User WHERE login = ?1")
    User getUserByLogin(@NotNull String login);

    boolean existsByLogin(@NotNull String login);

}