package ru.renessans.jvschool.volkov.task.manager.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.util.StringUtils;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.hash.InvalidSignatureCycleException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.hash.InvalidSignatureObjectException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.hash.InvalidSignatureSaltException;

import java.util.Objects;

@UtilityClass
public class SignatureUtil {

    @NotNull
    @SneakyThrows
    public String getHashSignature(
            @Nullable final Object object,
            @Nullable final String salt,
            @Nullable final Integer cycle
    ) {
        if (Objects.isNull(object)) throw new InvalidSignatureObjectException();
        if (StringUtils.isEmpty(salt)) throw new InvalidSignatureSaltException();
        if (StringUtils.isEmpty(cycle)) throw new InvalidSignatureCycleException();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(object);
        return getHashSignature(json, salt, cycle);
    }

    @NotNull
    @SneakyThrows
    public String getHashSignature(
            @Nullable final String line,
            @Nullable final String salt,
            @Nullable final Integer cycle
    ) {
        if (StringUtils.isEmpty(line)) throw new InvalidSignatureObjectException();
        if (StringUtils.isEmpty(salt)) throw new InvalidSignatureSaltException();
        if (StringUtils.isEmpty(cycle)) throw new InvalidSignatureCycleException();

        @NotNull String hashLine = line;
        for (int i = 0; i < cycle; i++) {
            hashLine = HashUtil.getHashLine(salt + hashLine + salt);
        }
        return hashLine;
    }

}