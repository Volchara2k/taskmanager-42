package ru.renessans.jvschool.volkov.task.manager.exception.invalid.service;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class InvalidPropertyKeyException extends AbstractException {

    @NotNull
    private static final String EMPTY_KEY = "Ошибка! Параметр \"ключ свойства\" отсутствует!\n";

    public InvalidPropertyKeyException() {
        super(EMPTY_KEY);
    }

}