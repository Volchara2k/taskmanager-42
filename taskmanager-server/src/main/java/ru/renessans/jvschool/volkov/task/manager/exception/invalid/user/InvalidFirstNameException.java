package ru.renessans.jvschool.volkov.task.manager.exception.invalid.user;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class InvalidFirstNameException extends AbstractException {

    @NotNull
    private static final String EMPTY_FIRST_NAME = "Ошибка! Параметр \"имя\" отсутствует!\n";

    public InvalidFirstNameException() {
        super(EMPTY_FIRST_NAME);
    }

}