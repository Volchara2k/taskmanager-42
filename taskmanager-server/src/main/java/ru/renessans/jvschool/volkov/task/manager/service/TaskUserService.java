package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import ru.renessans.jvschool.volkov.task.manager.api.service.IProjectUserService;
import ru.renessans.jvschool.volkov.task.manager.api.service.ITaskUserService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.exception.illegal.IllegalGetProjectException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidDescriptionException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidTitleException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidUserIdException;
import ru.renessans.jvschool.volkov.task.manager.model.Project;
import ru.renessans.jvschool.volkov.task.manager.model.Task;
import ru.renessans.jvschool.volkov.task.manager.repository.ITaskUserRepository;

import java.util.Objects;

@Service
@Transactional
public final class TaskUserService extends AbstractUserOwnerService<Task> implements ITaskUserService {

    @NotNull
    private final IProjectUserService projectUserService;

    public TaskUserService(
            @NotNull final ITaskUserRepository taskUserRepository,
            @NotNull final IUserService userService,
            @NotNull final IProjectUserService projectUserService
    ) {
        super(taskUserRepository, userService);
        this.projectUserService = projectUserService;
    }

    @NotNull
    @SneakyThrows
    @Override
    public Task addOwnerUser(
            @Nullable final String userId,
            @Nullable final String projectTitle,
            @Nullable final String title,
            @Nullable final String description
    ) {
        if (StringUtils.isEmpty(userId)) throw new InvalidUserIdException();
        if (StringUtils.isEmpty(projectTitle)) throw new InvalidTitleException();
        if (StringUtils.isEmpty(title)) throw new InvalidTitleException();
        if (StringUtils.isEmpty(description)) throw new InvalidDescriptionException();

        @Nullable final Project project = this.projectUserService.getOwnerUserByTitle(userId, projectTitle);
        if (Objects.isNull(project)) throw new IllegalGetProjectException();
        @NotNull final Task task = new Task(userId, title, description);
        task.setProject(project);

        return super.addOwnerUser(task);
    }

    @NotNull
    @SneakyThrows
    @Override
    public Task addOwnerUser(
            @Nullable final String userId,
            @Nullable final String title,
            @Nullable final String description
    ) {
        if (StringUtils.isEmpty(userId)) throw new InvalidUserIdException();
        if (StringUtils.isEmpty(title)) throw new InvalidTitleException();
        if (StringUtils.isEmpty(description)) throw new InvalidDescriptionException();
        @NotNull final Task task = new Task(userId, title, description);
        return super.addOwnerUser(task);
    }

}