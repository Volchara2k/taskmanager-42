package ru.renessans.jvschool.volkov.task.manager.casedata;

public final class CaseDataHashUtilProvider {

    @SuppressWarnings("unused")
    public Object[] validSaltHashLinesCaseData() {
        return new Object[]{
                new Object[]{"28152bc7b2805b68b76cd1677d0c626d", "null"},
                new Object[]{"3745a2ac38ccb2eaef7ffd3824ec1631", "    1"},
                new Object[]{"b3f577bc3a51a141b688665823ac50f2", "dwa       dwa"},
                new Object[]{"9adfd007390939d35c8d0bf5d8277a3e", "1      "},
                new Object[]{"6f91044ade328190828eed229cc5dd88", "#!@  "}
        };
    }

    @SuppressWarnings("unused")
    public Object[] validHashLinesCaseData() {
        return new Object[]{
                new Object[]{"37a6259cc0c1dae299a7866489dff0bd", "null"},
                new Object[]{"5ccb43dc4102d7d8dbd691cb424f949f", "    1"},
                new Object[]{"6b1a8f9d528dfb8407c3d5340f6d554f", "dwa       dwa"},
                new Object[]{"bd9a6f9e82e3b5a3546b77ff142b01ec", "1      "},
                new Object[]{"3b6f5ab5e3dd97d3e3334d4f6afef1fb", "#!@  "}
        };
    }

}