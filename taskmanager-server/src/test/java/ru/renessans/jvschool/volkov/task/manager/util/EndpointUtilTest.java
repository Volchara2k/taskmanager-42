package ru.renessans.jvschool.volkov.task.manager.util;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.api.IEndpoint;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataEndpointUtilProvider;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.endpoint.InvalidEndpointException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.endpoint.InvalidEndpointsException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.endpoint.InvalidHostException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.endpoint.InvalidPortException;
import ru.renessans.jvschool.volkov.task.manager.marker.NegativeImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.UtilityImplementation;

import javax.xml.ws.Endpoint;
import java.util.Collection;

@RunWith(value = JUnitParamsRunner.class)
public final class EndpointUtilTest {

    @Test(expected = InvalidEndpointException.class)
    @TestCaseName("Run testNegativePublishObjectWithoutImplementor for publish(null, \"127.0.0.1\", 9108)")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    public void testNegativePublishObjectWithoutImplementor() {
        @NotNull final String host = "127.0.0.1";
        Assert.assertNotNull(host);
        @NotNull final Integer port = 9108;
        Assert.assertNotNull(port);
        EndpointUtil.publish((@Nullable IEndpoint) null, host, port);
    }

    @Test(expected = InvalidHostException.class)
    @Ignore
    @TestCaseName("Run testNegativePublishObjectWithoutHost for publish({0}, \"{1}\", {2})")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataEndpointUtilProvider.class,
            method = "invalidHostsCaseData"
    )
    public void testNegativePublishObjectWithoutHost(
            @NotNull final IEndpoint implementor,
            @Nullable final String host,
            @NotNull final Integer port
    ) {
        Assert.assertNotNull(implementor);
        Assert.assertNotNull(port);
        EndpointUtil.publish(implementor, host, port);
    }

    @Test(expected = InvalidPortException.class)
    @Ignore
    @TestCaseName("Run testNegativePublishObjectWithoutPort for publish({0}, \"{1}\", {2})")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataEndpointUtilProvider.class,
            method = "invalidPortsCaseData"
    )
    public void testNegativePublishObjectWithoutPort(
            @NotNull final IEndpoint implementor,
            @NotNull final String host,
            @Nullable final Integer port
    ) {
        Assert.assertNotNull(implementor);
        Assert.assertNotNull(host);
        EndpointUtil.publish(implementor, host, port);
    }

    @Test(expected = InvalidEndpointsException.class)
    @Ignore
    @TestCaseName("Run testNegativePublishObjectsWithoutImplementor for publish({0}, \"{1}\", {2})")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataEndpointUtilProvider.class,
            method = "invalidObjectsCaseData"
    )
    public void testNegativePublishObjectsWithoutImplementor(
            @Nullable final Collection<IEndpoint> implementors,
            @NotNull final String host,
            @NotNull final Integer port
    ) {
        Assert.assertNotNull(host);
        Assert.assertNotNull(port);
        EndpointUtil.publish(implementors, host, port);
    }

    @Test(expected = InvalidHostException.class)
    @Ignore
    @TestCaseName("Run testNegativePublishObjectsWithoutHost for publish({0}, \"{1}\", {2})")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataEndpointUtilProvider.class,
            method = "invalidHostsObjectsCaseData"
    )
    public void testNegativePublishObjectsWithoutHost(
            @NotNull final Collection<IEndpoint> implementors,
            @Nullable final String host,
            @NotNull final Integer port
    ) {
        Assert.assertNotNull(implementors);
        Assert.assertNotNull(port);
        EndpointUtil.publish(implementors, host, port);
    }

    @Test(expected = InvalidPortException.class)
    @Ignore
    @TestCaseName("Run testNegativePublishObjectsWithoutPort for publish({0}, \"{1}\", {2})")
    @Category({NegativeImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataEndpointUtilProvider.class,
            method = "invalidPortsObjectsCaseData"
    )
    public void testNegativePublishObjectsWithoutPort(
            @NotNull final Collection<IEndpoint> implementors,
            @NotNull final String host,
            @Nullable final Integer port
    ) {
        Assert.assertNotNull(implementors);
        Assert.assertNotNull(host);
        EndpointUtil.publish(implementors, host, port);
    }

    @Test
    @Ignore
    @TestCaseName("Run testPublishObject for publish({0}, \"{1}\", {2})")
    @Category({PositiveImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataEndpointUtilProvider.class,
            method = "validObjectCaseData"
    )
    public void testPublishObject(
            @NotNull final IEndpoint implementor,
            @NotNull final String host,
            @NotNull final Integer port
    ) {
        Assert.assertNotNull(implementor);
        Assert.assertNotNull(host);
        Assert.assertNotNull(port);
        @NotNull final Endpoint endpoint = EndpointUtil.publish(implementor, host, port);
        Assert.assertNotNull(endpoint);
    }

    @Test
    @Ignore
    @TestCaseName("Run testPublishObjects for publish({0}, \"{1}\", {2})")
    @Category({PositiveImplementation.class, UtilityImplementation.class})
    @Parameters(
            source = CaseDataEndpointUtilProvider.class,
            method = "validObjectsCaseData"
    )
    public void testPublishObjects(
            @NotNull final Collection<IEndpoint> implementors,
            @NotNull final String host,
            @NotNull final Integer port
    ) {
        Assert.assertNotNull(implementors);
        Assert.assertNotNull(host);
        Assert.assertNotNull(port);
        @NotNull final Collection<Endpoint> publishEndpoints = EndpointUtil.publish(implementors, host, port);
        Assert.assertNotNull(publishEndpoints);
        Assert.assertNotEquals(0, publishEndpoints.size());
    }

}