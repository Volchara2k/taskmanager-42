package ru.renessans.jvschool.volkov.task.manager.endpoint;

import junitparams.JUnitParamsRunner;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.marker.EndpointImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;

@RunWith(value = JUnitParamsRunner.class)
@Category({PositiveImplementation.class, EndpointImplementation.class})
public final class AdminDataInterChangeEndpointTest {

//    @NotNull
//    private final IAdminDataInterChangeEndpoint adminDataInterChangeEndpoint = new AdminDataInterChangeEndpoint();

//    @Before
//    public void initialAdminRecordBefore() {
//        @NotNull final User addAdminRecord = this.userService.addUser(
//                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD, UserRole.ADMIN
//        );
//        Assert.assertNotNull(addAdminRecord);
//    }
//
//    @Before
//    public void createTempFilesBefore() {
//        Assert.assertNotNull(this.configService);
//        this.configService.load();
//        this.serviceLocator.getEntityManagerService().build();
//        FileUtil.create(this.configService.getBase64Pathname());
//        FileUtil.create(this.configService.getBinPathname());
//        FileUtil.create(this.configService.getBase64Pathname());
//        FileUtil.create(this.configService.getJsonPathname());
//        FileUtil.create(this.configService.getXmlPathname());
//        FileUtil.create(this.configService.getYamlPathname());
//    }
//
//    @After
//    public void clearTempFilesAfter() {
//        Assert.assertNotNull(this.configService);
//        FileUtil.delete(this.configService.getBase64Pathname());
//        FileUtil.delete(this.configService.getBinPathname());
//        FileUtil.delete(this.configService.getBase64Pathname());
//        FileUtil.delete(this.configService.getJsonPathname());
//        FileUtil.delete(this.configService.getXmlPathname());
//        FileUtil.delete(this.configService.getYamlPathname());
//    }
//
//    @Test
//    @TestCaseName("Run testDataBinClear for dataBinClear(session)")
//    public void testDataBinClear() {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.adminDataInterChangeEndpoint);
//        @NotNull final Session open = this.sessionService.openSession(
//                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        final boolean isBinClear = this.adminDataInterChangeEndpoint.dataBinClear(conversion);
//        Assert.assertTrue(isBinClear);
//    }
//
//    @Test
//    @TestCaseName("Run testDataBase64Clear for dataBase64Clear(session)")
//    public void testDataBase64Clear() {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.adminDataInterChangeEndpoint);
//        @NotNull final Session open = this.sessionService.openSession(
//                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        final boolean isBase64Clear = this.adminDataInterChangeEndpoint.dataBase64Clear(conversion);
//        Assert.assertTrue(isBase64Clear);
//    }
//
//    @Test
//    @TestCaseName("Run testDataJsonClear for dataJsonClear(session)")
//    public void testDataJsonClear() {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.adminDataInterChangeEndpoint);
//        @NotNull final Session open = this.sessionService.openSession(
//                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        final boolean isJsonClear = this.adminDataInterChangeEndpoint.dataJsonClear(conversion);
//        Assert.assertTrue(isJsonClear);
//    }
//
//    @Test
//    @TestCaseName("Run testDataXmlClear for dataXmlClear(session)")
//    public void testDataXmlClear() {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.adminDataInterChangeEndpoint);
//        @NotNull final Session open = this.sessionService.openSession(
//                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        final boolean isXmlClear = this.adminDataInterChangeEndpoint.dataXmlClear(conversion);
//        Assert.assertTrue(isXmlClear);
//    }
//
//    @Test
//    @TestCaseName("Run testDataYamlClear for dataYamlClear(session)")
//    public void testDataYamlClear() {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.sessionAdapter);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(this.adminDataInterChangeEndpoint);
//        @NotNull final Session open = this.sessionService.openSession(
//                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        final boolean isYamlClear = this.adminDataInterChangeEndpoint.dataYamlClear(conversion);
//        Assert.assertTrue(isYamlClear);
//    }
//
//    @Test
//    @TestCaseName("Run testExportDataBin for exportDataBin(session)")
//    @Parameters(
//            source = CaseDomainProvider.class,
//            method = "validCollectionDomainsCaseData"
//    )
//    public void testExportDataBin(
//            @NotNull final DomainDTO data
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.taskService);
//        Assert.assertNotNull(this.projectService);
//        Assert.assertNotNull(this.adminDataInterChangeEndpoint);
//        @NotNull final Collection<User> conversionUsers =
//                data.getUsers().stream().map(this.userAdapter::toModel).collect(Collectors.toList());
//        Assert.assertNotNull(conversionUsers);
//        @NotNull final Collection<User> setAllUserRecords = this.userService.setAllRecords(conversionUsers);
//        Assert.assertNotNull(setAllUserRecords);
//        @NotNull final Collection<Task> conversionTasks =
//                data.getTasks().stream().map(this.taskAdapter::toModel).collect(Collectors.toList());
//        @NotNull final Collection<Task> setAllTaskRecords = this.taskService.setAllRecords(conversionTasks);
//        Assert.assertNotNull(setAllTaskRecords);
//        @NotNull final Collection<Project> conversionProjects =
//                data.getProjects().stream().map(this.projectAdapter::toModel).collect(Collectors.toList());
//        @NotNull final Collection<Project> setAllProjectRecords = this.projectService.setAllRecords(conversionProjects);
//        Assert.assertNotNull(setAllProjectRecords);
//        @NotNull final User addRecord = this.authService.signUp(
//                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD, UserRole.ADMIN
//        );
//        Assert.assertNotNull(addRecord);
//        setAllUserRecords.add(addRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @NotNull final DomainDTO domain = this.adminDataInterChangeEndpoint.exportDataBin(conversion);
//        Assert.assertNotNull(domain);
//        Assert.assertNotEquals(0, domain.getUsers().size());
//        Assert.assertNotEquals(0, domain.getTasks().size());
//        Assert.assertNotEquals(0, domain.getProjects().size());
//        Assert.assertEquals(domain.getUsers().toString(), this.userService.getAllRecords().toString());
//        Assert.assertEquals(domain.getProjects().toString(), this.projectService.getAllRecords().toString());
//        Assert.assertEquals(domain.getTasks().toString(), this.taskService.getAllRecords().toString());
//    }
//
//    @Test
//    @TestCaseName("Run testExportDataBase64 for exportDataBase64(session)")
//    @Parameters(
//            source = CaseDomainProvider.class,
//            method = "validCollectionDomainsCaseData"
//    )
//    public void testExportDataBase64(
//            @NotNull final DomainDTO data
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.taskService);
//        Assert.assertNotNull(this.projectService);
//        Assert.assertNotNull(this.adminDataInterChangeEndpoint);
//        @NotNull final Collection<User> conversionUsers =
//                data.getUsers().stream().map(this.userAdapter::toModel).collect(Collectors.toList());
//        Assert.assertNotNull(conversionUsers);
//        @NotNull final Collection<User> setAllUserRecords = this.userService.setAllRecords(conversionUsers);
//        Assert.assertNotNull(setAllUserRecords);
//        @NotNull final Collection<Task> conversionTasks =
//                data.getTasks().stream().map(this.taskAdapter::toModel).collect(Collectors.toList());
//        @NotNull final Collection<Task> setAllTaskRecords = this.taskService.setAllRecords(conversionTasks);
//        Assert.assertNotNull(setAllTaskRecords);
//        @NotNull final Collection<Project> conversionProjects =
//                data.getProjects().stream().map(this.projectAdapter::toModel).collect(Collectors.toList());
//        @NotNull final Collection<Project> setAllProjectRecords = this.projectService.setAllRecords(conversionProjects);
//        Assert.assertNotNull(setAllProjectRecords);
//        Assert.assertNotNull(setAllProjectRecords);
//        @NotNull final User addRecord = this.authService.signUp(
//                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD, UserRole.ADMIN
//        );
//        Assert.assertNotNull(addRecord);
//        setAllUserRecords.add(addRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @NotNull final DomainDTO domain = this.adminDataInterChangeEndpoint.exportDataBase64(conversion);
//        Assert.assertNotNull(domain);
//        Assert.assertNotEquals(0, domain.getUsers().size());
//        Assert.assertNotEquals(0, domain.getTasks().size());
//        Assert.assertNotEquals(0, domain.getProjects().size());
//        Assert.assertEquals(domain.getUsers().toString(), this.userService.getAllRecords().toString());
//        Assert.assertEquals(domain.getProjects().toString(), this.projectService.getAllRecords().toString());
//        Assert.assertEquals(domain.getTasks().toString(), this.taskService.getAllRecords().toString());
//    }
//
//    @Test
//    @TestCaseName("Run testExportDataJson for exportDataJson(session)")
//    @Parameters(
//            source = CaseDomainProvider.class,
//            method = "validCollectionDomainsCaseData"
//    )
//    public void testExportDataJson(
//            @NotNull final DomainDTO data
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.taskService);
//        Assert.assertNotNull(this.projectService);
//        Assert.assertNotNull(this.adminDataInterChangeEndpoint);
//        @NotNull final Collection<User> conversionUsers =
//                data.getUsers().stream().map(this.userAdapter::toModel).collect(Collectors.toList());
//        Assert.assertNotNull(conversionUsers);
//        @NotNull final Collection<User> setAllUserRecords = this.userService.setAllRecords(conversionUsers);
//        Assert.assertNotNull(setAllUserRecords);
//        @NotNull final Collection<Task> conversionTasks =
//                data.getTasks().stream().map(this.taskAdapter::toModel).collect(Collectors.toList());
//        @NotNull final Collection<Task> setAllTaskRecords = this.taskService.setAllRecords(conversionTasks);
//        Assert.assertNotNull(setAllTaskRecords);
//        @NotNull final Collection<Project> conversionProjects =
//                data.getProjects().stream().map(this.projectAdapter::toModel).collect(Collectors.toList());
//        @NotNull final Collection<Project> setAllProjectRecords = this.projectService.setAllRecords(conversionProjects);
//        Assert.assertNotNull(setAllProjectRecords);
//        @NotNull final User addRecord = this.authService.signUp(
//                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD, UserRole.ADMIN
//        );
//        Assert.assertNotNull(addRecord);
//        setAllUserRecords.add(addRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @NotNull final DomainDTO domain = this.adminDataInterChangeEndpoint.exportDataJson(conversion);
//        Assert.assertNotNull(domain);
//        Assert.assertNotEquals(0, domain.getUsers().size());
//        Assert.assertNotEquals(0, domain.getTasks().size());
//        Assert.assertNotEquals(0, domain.getProjects().size());
//        Assert.assertEquals(domain.getUsers().toString(), this.userService.getAllRecords().toString());
//        Assert.assertEquals(domain.getProjects().toString(), this.projectService.getAllRecords().toString());
//        Assert.assertEquals(domain.getTasks().toString(), this.taskService.getAllRecords().toString());
//    }
//
//    @Test
//    @TestCaseName("Run testExportDataXml for exportDataXml(session)")
//    @Parameters(
//            source = CaseDomainProvider.class,
//            method = "validCollectionDomainsCaseData"
//    )
//    public void testExportDataXml(
//            @NotNull final DomainDTO data
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.taskService);
//        Assert.assertNotNull(this.projectService);
//        Assert.assertNotNull(this.adminDataInterChangeEndpoint);
//        @NotNull final Collection<User> conversionUsers =
//                data.getUsers().stream().map(this.userAdapter::toModel).collect(Collectors.toList());
//        Assert.assertNotNull(conversionUsers);
//        @NotNull final Collection<User> setAllUserRecords = this.userService.setAllRecords(conversionUsers);
//        Assert.assertNotNull(setAllUserRecords);
//        @NotNull final Collection<Task> conversionTasks =
//                data.getTasks().stream().map(this.taskAdapter::toModel).collect(Collectors.toList());
//        @NotNull final Collection<Task> setAllTaskRecords = this.taskService.setAllRecords(conversionTasks);
//        Assert.assertNotNull(setAllTaskRecords);
//        @NotNull final Collection<Project> conversionProjects =
//                data.getProjects().stream().map(this.projectAdapter::toModel).collect(Collectors.toList());
//        @NotNull final Collection<Project> setAllProjectRecords = this.projectService.setAllRecords(conversionProjects);
//        Assert.assertNotNull(setAllProjectRecords);
//        @NotNull final User addRecord = this.authService.signUp(
//                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD, UserRole.ADMIN
//        );
//        Assert.assertNotNull(addRecord);
//        setAllUserRecords.add(addRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @NotNull final DomainDTO domain = this.adminDataInterChangeEndpoint.exportDataXml(conversion);
//        Assert.assertNotNull(domain);
//        Assert.assertNotEquals(0, domain.getUsers().size());
//        Assert.assertNotEquals(0, domain.getTasks().size());
//        Assert.assertNotEquals(0, domain.getProjects().size());
//        Assert.assertEquals(domain.getUsers().toString(), this.userService.getAllRecords().toString());
//        Assert.assertEquals(domain.getProjects().toString(), this.projectService.getAllRecords().toString());
//        Assert.assertEquals(domain.getTasks().toString(), this.taskService.getAllRecords().toString());
//    }
//
//    @Test
//    @TestCaseName("Run testExportDataYaml for exportDataYaml(session)")
//    @Parameters(
//            source = CaseDomainProvider.class,
//            method = "validCollectionDomainsCaseData"
//    )
//    public void testExportDataYaml(
//            @NotNull final DomainDTO data
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.taskService);
//        Assert.assertNotNull(this.projectService);
//        Assert.assertNotNull(this.adminDataInterChangeEndpoint);
//        @NotNull final Collection<User> conversionUsers =
//                data.getUsers().stream().map(this.userAdapter::toModel).collect(Collectors.toList());
//        Assert.assertNotNull(conversionUsers);
//        @NotNull final Collection<User> setAllUserRecords = this.userService.setAllRecords(conversionUsers);
//        Assert.assertNotNull(setAllUserRecords);
//        @NotNull final Collection<Task> conversionTasks =
//                data.getTasks().stream().map(this.taskAdapter::toModel).collect(Collectors.toList());
//        @NotNull final Collection<Task> setAllTaskRecords = this.taskService.setAllRecords(conversionTasks);
//        Assert.assertNotNull(setAllTaskRecords);
//        @NotNull final Collection<Project> conversionProjects =
//                data.getProjects().stream().map(this.projectAdapter::toModel).collect(Collectors.toList());
//        @NotNull final Collection<Project> setAllProjectRecords = this.projectService.setAllRecords(conversionProjects);
//        Assert.assertNotNull(setAllProjectRecords);
//        @NotNull final User addRecord = this.authService.signUp(
//                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD, UserRole.ADMIN
//        );
//        Assert.assertNotNull(addRecord);
//        setAllUserRecords.add(addRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @NotNull final DomainDTO domain = this.adminDataInterChangeEndpoint.exportDataYaml(conversion);
//        Assert.assertNotNull(domain);
//        Assert.assertNotEquals(0, domain.getUsers().size());
//        Assert.assertNotEquals(0, domain.getTasks().size());
//        Assert.assertNotEquals(0, domain.getProjects().size());
//        Assert.assertEquals(domain.getUsers().toString(), this.userService.getAllRecords().toString());
//        Assert.assertEquals(domain.getProjects().toString(), this.projectService.getAllRecords().toString());
//        Assert.assertEquals(domain.getTasks().toString(), this.taskService.getAllRecords().toString());
//    }
//
//    @Test
//    @TestCaseName("Run testImportDataBin for importDataBin(open)")
//    @Parameters(
//            source = CaseDomainProvider.class,
//            method = "validCollectionDomainsCaseData"
//    )
//    public void testImportDataBin(
//            @NotNull final DomainDTO data
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.taskService);
//        Assert.assertNotNull(this.projectService);
//        Assert.assertNotNull(this.adminDataInterChangeEndpoint);
//        @NotNull final Collection<User> conversionUsers =
//                data.getUsers().stream().map(this.userAdapter::toModel).collect(Collectors.toList());
//        Assert.assertNotNull(conversionUsers);
//        @NotNull final Collection<User> setAllUserRecords = this.userService.setAllRecords(conversionUsers);
//        Assert.assertNotNull(setAllUserRecords);
//        @NotNull final Collection<Task> conversionTasks =
//                data.getTasks().stream().map(this.taskAdapter::toModel).collect(Collectors.toList());
//        @NotNull final Collection<Task> setAllTaskRecords = this.taskService.setAllRecords(conversionTasks);
//        Assert.assertNotNull(setAllTaskRecords);
//        @NotNull final Collection<Project> conversionProjects =
//                data.getProjects().stream().map(this.projectAdapter::toModel).collect(Collectors.toList());
//        @NotNull final Collection<Project> setAllProjectRecords = this.projectService.setAllRecords(conversionProjects);
//        Assert.assertNotNull(setAllProjectRecords);
//        @NotNull final User addRecord = this.authService.signUp(
//                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD, UserRole.ADMIN
//        );
//        Assert.assertNotNull(addRecord);
//        setAllUserRecords.add(addRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @NotNull final DomainDTO domain = this.adminDataInterChangeEndpoint.exportDataBin(conversion);
//        Assert.assertNotNull(domain);
//        @NotNull final DomainDTO importDataBin = this.adminDataInterChangeEndpoint.importDataBin(conversion);
//        Assert.assertNotNull(importDataBin);
//        Assert.assertNotEquals(0, domain.getUsers().size());
//        Assert.assertNotEquals(0, domain.getTasks().size());
//        Assert.assertNotEquals(0, domain.getProjects().size());
//        Assert.assertEquals(importDataBin.getUsers().toString(), this.userService.getAllRecords().toString());
//        Assert.assertEquals(importDataBin.getProjects().toString(), this.projectService.getAllRecords().toString());
//        Assert.assertEquals(importDataBin.getTasks().toString(), this.taskService.getAllRecords().toString());
//    }
//
//    @Test
//    @TestCaseName("Run testImportDataBase64 for importDataBase64(session)")
//    @Parameters(
//            source = CaseDomainProvider.class,
//            method = "validCollectionDomainsCaseData"
//    )
//    public void testImportDataBase64(
//            @NotNull final DomainDTO data
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.taskService);
//        Assert.assertNotNull(this.projectService);
//        Assert.assertNotNull(this.adminDataInterChangeEndpoint);
//        @NotNull final Collection<User> conversionUsers =
//                data.getUsers().stream().map(this.userAdapter::toModel).collect(Collectors.toList());
//        Assert.assertNotNull(conversionUsers);
//        @NotNull final Collection<User> setAllUserRecords = this.userService.setAllRecords(conversionUsers);
//        Assert.assertNotNull(setAllUserRecords);
//        @NotNull final Collection<Task> conversionTasks =
//                data.getTasks().stream().map(this.taskAdapter::toModel).collect(Collectors.toList());
//        @NotNull final Collection<Task> setAllTaskRecords = this.taskService.setAllRecords(conversionTasks);
//        Assert.assertNotNull(setAllTaskRecords);
//        @NotNull final Collection<Project> conversionProjects =
//                data.getProjects().stream().map(this.projectAdapter::toModel).collect(Collectors.toList());
//        @NotNull final Collection<Project> setAllProjectRecords = this.projectService.setAllRecords(conversionProjects);
//        Assert.assertNotNull(setAllProjectRecords);
//        @NotNull final User addRecord = this.authService.signUp(
//                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD, UserRole.ADMIN
//        );
//        Assert.assertNotNull(addRecord);
//        setAllUserRecords.add(addRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @NotNull final DomainDTO domain = this.adminDataInterChangeEndpoint.exportDataBase64(conversion);
//        Assert.assertNotNull(domain);
//        @NotNull final DomainDTO importDataBase64 = this.adminDataInterChangeEndpoint.importDataBase64(conversion);
//        Assert.assertNotNull(importDataBase64);
//        Assert.assertNotEquals(0, domain.getUsers().size());
//        Assert.assertNotEquals(0, domain.getTasks().size());
//        Assert.assertNotEquals(0, domain.getProjects().size());
//        Assert.assertEquals(importDataBase64.getUsers().toString(), this.userService.getAllRecords().toString());
//        Assert.assertEquals(importDataBase64.getProjects().toString(), this.projectService.getAllRecords().toString());
//        Assert.assertEquals(importDataBase64.getTasks().toString(), this.taskService.getAllRecords().toString());
//    }
//
//    @Test
//    @TestCaseName("Run testImportDataJson for importDataJson(session)")
//    @Parameters(
//            source = CaseDomainProvider.class,
//            method = "validCollectionDomainsCaseData"
//    )
//    public void testImportDataJson(
//            @NotNull final DomainDTO data
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.taskService);
//        Assert.assertNotNull(this.projectService);
//        Assert.assertNotNull(this.adminDataInterChangeEndpoint);
//        @NotNull final Collection<User> conversionUsers =
//                data.getUsers().stream().map(this.userAdapter::toModel).collect(Collectors.toList());
//        Assert.assertNotNull(conversionUsers);
//        @NotNull final Collection<User> setAllUserRecords = this.userService.setAllRecords(conversionUsers);
//        Assert.assertNotNull(setAllUserRecords);
//        @NotNull final Collection<Task> conversionTasks =
//                data.getTasks().stream().map(this.taskAdapter::toModel).collect(Collectors.toList());
//        @NotNull final Collection<Task> setAllTaskRecords = this.taskService.setAllRecords(conversionTasks);
//        Assert.assertNotNull(setAllTaskRecords);
//        @NotNull final Collection<Project> conversionProjects =
//                data.getProjects().stream().map(this.projectAdapter::toModel).collect(Collectors.toList());
//        @NotNull final Collection<Project> setAllProjectRecords = this.projectService.setAllRecords(conversionProjects);
//        Assert.assertNotNull(setAllProjectRecords);
//        @NotNull final User addRecord = this.authService.signUp(
//                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD, UserRole.ADMIN
//        );
//        Assert.assertNotNull(addRecord);
//        setAllUserRecords.add(addRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @NotNull final DomainDTO domain = this.adminDataInterChangeEndpoint.exportDataJson(conversion);
//        Assert.assertNotNull(domain);
//        @NotNull final DomainDTO importDataJson = this.adminDataInterChangeEndpoint.importDataJson(conversion);
//        Assert.assertNotNull(importDataJson);
//        Assert.assertNotEquals(0, domain.getUsers().size());
//        Assert.assertNotEquals(0, domain.getTasks().size());
//        Assert.assertNotEquals(0, domain.getProjects().size());
//        Assert.assertEquals(importDataJson.getUsers().toString(), this.userService.getAllRecords().toString());
//        Assert.assertEquals(importDataJson.getProjects().toString(), this.projectService.getAllRecords().toString());
//        Assert.assertEquals(importDataJson.getTasks().toString(), this.taskService.getAllRecords().toString());
//    }
//
//    @Test
//    @TestCaseName("Run testImportDataXml for importDataXml(session)")
//    @Parameters(
//            source = CaseDomainProvider.class,
//            method = "validCollectionDomainsCaseData"
//    )
//    public void testImportDataXml(
//            @NotNull final DomainDTO data
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.taskService);
//        Assert.assertNotNull(this.projectService);
//        Assert.assertNotNull(this.adminDataInterChangeEndpoint);
//        @NotNull final Collection<User> conversionUsers =
//                data.getUsers().stream().map(this.userAdapter::toModel).collect(Collectors.toList());
//        Assert.assertNotNull(conversionUsers);
//        @NotNull final Collection<User> setAllUserRecords = this.userService.setAllRecords(conversionUsers);
//        Assert.assertNotNull(setAllUserRecords);
//        @NotNull final Collection<Task> conversionTasks =
//                data.getTasks().stream().map(this.taskAdapter::toModel).collect(Collectors.toList());
//        @NotNull final Collection<Task> setAllTaskRecords = this.taskService.setAllRecords(conversionTasks);
//        Assert.assertNotNull(setAllTaskRecords);
//        @NotNull final Collection<Project> conversionProjects =
//                data.getProjects().stream().map(this.projectAdapter::toModel).collect(Collectors.toList());
//        @NotNull final Collection<Project> setAllProjectRecords = this.projectService.setAllRecords(conversionProjects);
//        Assert.assertNotNull(setAllProjectRecords);
//        @NotNull final User addRecord = this.authService.signUp(
//                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD, UserRole.ADMIN
//        );
//        Assert.assertNotNull(addRecord);
//        setAllUserRecords.add(addRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @NotNull final DomainDTO domain = this.adminDataInterChangeEndpoint.exportDataXml(conversion);
//        Assert.assertNotNull(domain);
//        @NotNull final DomainDTO importDataXml = this.adminDataInterChangeEndpoint.importDataXml(conversion);
//        Assert.assertNotNull(importDataXml);
//        Assert.assertNotEquals(0, domain.getUsers().size());
//        Assert.assertNotEquals(0, domain.getTasks().size());
//        Assert.assertNotEquals(0, domain.getProjects().size());
//        Assert.assertEquals(importDataXml.getUsers().toString(), this.userService.getAllRecords().toString());
//        Assert.assertEquals(importDataXml.getProjects().toString(), this.projectService.getAllRecords().toString());
//        Assert.assertEquals(importDataXml.getTasks().toString(), this.taskService.getAllRecords().toString());
//    }
//
//    @Test
//    @TestCaseName("Run testImportDataYaml for importDataYaml(session)")
//    @Parameters(
//            source = CaseDomainProvider.class,
//            method = "validCollectionDomainsCaseData"
//    )
//    public void testImportDataYaml(
//            @NotNull final DomainDTO data
//    ) {
//        Assert.assertNotNull(this.serviceLocatorRepository);
//        Assert.assertNotNull(this.serviceLocator);
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.taskService);
//        Assert.assertNotNull(this.projectService);
//        Assert.assertNotNull(this.adminDataInterChangeEndpoint);
//        @NotNull final Collection<User> conversionUsers =
//                data.getUsers().stream().map(this.userAdapter::toModel).collect(Collectors.toList());
//        Assert.assertNotNull(conversionUsers);
//        @NotNull final Collection<User> setAllUserRecords = this.userService.setAllRecords(conversionUsers);
//        Assert.assertNotNull(setAllUserRecords);
//        @NotNull final Collection<Task> conversionTasks =
//                data.getTasks().stream().map(this.taskAdapter::toModel).collect(Collectors.toList());
//        @NotNull final Collection<Task> setAllTaskRecords = this.taskService.setAllRecords(conversionTasks);
//        Assert.assertNotNull(setAllTaskRecords);
//        @NotNull final Collection<Project> conversionProjects =
//                data.getProjects().stream().map(this.projectAdapter::toModel).collect(Collectors.toList());
//        @NotNull final Collection<Project> setAllProjectRecords = this.projectService.setAllRecords(conversionProjects);
//        Assert.assertNotNull(setAllProjectRecords);
//        @NotNull final User addRecord = this.authService.signUp(
//                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD, UserRole.ADMIN
//        );
//        Assert.assertNotNull(addRecord);
//        setAllUserRecords.add(addRecord);
//        @NotNull final Session open = this.sessionService.openSession(
//                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
//        );
//        Assert.assertNotNull(open);
//        @Nullable final SessionDTO conversion = this.sessionAdapter.toDTO(open);
//        Assert.assertNotNull(conversion);
//
//        @NotNull final DomainDTO domain = this.adminDataInterChangeEndpoint.exportDataYaml(conversion);
//        Assert.assertNotNull(domain);
//        @NotNull final DomainDTO importDataYaml = this.adminDataInterChangeEndpoint.importDataYaml(conversion);
//        Assert.assertNotNull(importDataYaml);
//        Assert.assertNotEquals(0, domain.getUsers().size());
//        Assert.assertNotEquals(0, domain.getTasks().size());
//        Assert.assertNotEquals(0, domain.getProjects().size());
//        Assert.assertEquals(importDataYaml.getUsers().toString(), this.userService.getAllRecords().toString());
//        Assert.assertEquals(importDataYaml.getProjects().toString(), this.projectService.getAllRecords().toString());
//        Assert.assertEquals(importDataYaml.getTasks().toString(), this.taskService.getAllRecords().toString());
//    }

}