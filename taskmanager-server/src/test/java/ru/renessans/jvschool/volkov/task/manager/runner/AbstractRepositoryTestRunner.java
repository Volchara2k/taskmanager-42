package ru.renessans.jvschool.volkov.task.manager.runner;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import ru.renessans.jvschool.volkov.task.manager.repository.ProjectUserRepositoryTest;
import ru.renessans.jvschool.volkov.task.manager.repository.SessionRepositoryTest;
import ru.renessans.jvschool.volkov.task.manager.repository.TaskUserRepositoryTest;
import ru.renessans.jvschool.volkov.task.manager.repository.UserRepositoryTest;

@RunWith(Suite.class)
@Suite.SuiteClasses(
        {
                ProjectUserRepositoryTest.class,
                SessionRepositoryTest.class,
                TaskUserRepositoryTest.class,
                UserRepositoryTest.class
        }
)

public abstract class AbstractRepositoryTestRunner {
}