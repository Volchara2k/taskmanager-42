package ru.renessans.jvschool.volkov.task.manager.invoker.auth;

public enum OAuthFlow {
    accessCode, implicit, password, application
}