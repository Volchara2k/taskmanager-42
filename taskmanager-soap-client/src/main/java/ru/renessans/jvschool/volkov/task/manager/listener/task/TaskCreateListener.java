package ru.renessans.jvschool.volkov.task.manager.listener.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICookieService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.soap.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.soap.TaskSoapEndpoint;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalInputEvent;
import ru.renessans.jvschool.volkov.task.manager.util.BuildOwnerUser;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

import javax.xml.ws.Holder;

@Component
public class TaskCreateListener extends AbstractTaskListener {

    @NotNull
    private static final String CMD_TASK_CREATE = "task-create";

    @NotNull
    private static final String DESC_TASK_CREATE = "добавить новую задачу";

    @NotNull
    private static final String NOTIFY_TASK_CREATE =
            "Происходит попытка инициализации создания задачи. \n" +
                    "Для создания задачи введите заголовок проекта для задачи, заголовок задачи и описание. ";

    public TaskCreateListener(
            @NotNull final TaskSoapEndpoint taskEndpoint,
            @NotNull final ICookieService sessionService
    ) {
        super(taskEndpoint, sessionService);
    }

    @NotNull
    @Override
    public String command() {
        return CMD_TASK_CREATE;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_TASK_CREATE;
    }

    @SneakyThrows
    @Async
    @Override
    @EventListener(condition = "@taskCreateListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalInputEvent terminalEvent) {
        ViewUtil.print(NOTIFY_TASK_CREATE);
        @NotNull final String title = ViewUtil.getLine();
        @NotNull final String description = ViewUtil.getLine();
        @NotNull final TaskDTO taskDTO = BuildOwnerUser.buildTaskDTO(title, description);
        @NotNull final Holder<TaskDTO> holder = new Holder<>(taskDTO);
        super.sessionService.setListCookieRowRequest(super.taskEndpoint);
        super.taskEndpoint.addTask(holder);
    }

}