package ru.renessans.jvschool.volkov.task.manager.listener.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICookieService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.soap.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.soap.TaskSoapEndpoint;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalInputEvent;
import ru.renessans.jvschool.volkov.task.manager.util.BuildOwnerUser;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

import javax.xml.ws.Holder;

@Component
public class TaskUpdateListener extends AbstractTaskListener {

    @NotNull
    private static final String CMD_TASK_UPDATE_BY_ID = "task-update-by-id";

    @NotNull
    private static final String DESC_TASK_UPDATE_BY_ID = "обновить задачу по идентификатору";

    @NotNull
    private static final String NOTIFY_TASK_UPDATE_BY_ID =
            "Происходит попытка инициализации обновления задачи. \n" +
                    "Для обновления задачи по идентификатору введите идентификатор задачи из списка.\n" +
                    "Для обновления задачи введите её заголовок или заголовок с описанием. ";

    public TaskUpdateListener(
            @NotNull final TaskSoapEndpoint taskEndpoint,
            @NotNull final ICookieService sessionService
    ) {
        super(taskEndpoint, sessionService);
    }

    @NotNull
    @Override
    public String command() {
        return CMD_TASK_UPDATE_BY_ID;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_TASK_UPDATE_BY_ID;
    }

    @Async
    @Override
    @SneakyThrows
    @EventListener(condition = "@taskUpdateListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalInputEvent terminalEvent) {
        ViewUtil.print(NOTIFY_TASK_UPDATE_BY_ID);
        @NotNull final String id = ViewUtil.getLine();
        super.sessionService.setListCookieRowRequest(super.taskEndpoint);
        @NotNull final TaskDTO taskResponse = super.taskEndpoint.getTaskById(id);

        @NotNull final String title = ViewUtil.getLine();
        @NotNull final String description = ViewUtil.getLine();
        @NotNull final TaskDTO updateTaskDTO =
                BuildOwnerUser.buildTaskDTO(taskResponse, title, description);

        @NotNull final Holder<TaskDTO> holder = new Holder<>(updateTaskDTO);
        super.taskEndpoint.updateTask(holder);
    }

}