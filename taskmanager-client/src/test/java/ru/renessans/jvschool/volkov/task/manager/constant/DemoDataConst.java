package ru.renessans.jvschool.volkov.task.manager.constant;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DemoDataConst {

    @NotNull
    public static final String TASK_TITLE = "exceptions change";

    @NotNull
    public static final String TASK_DESCRIPTION = "change the approach to exceptions";

    @NotNull
    public static final String PROJECT_TITLE = "rethink view";

    @NotNull
    public static final String PROJECT_DESCRIPTION = "rethink the view approach";

    @NotNull
    public static final String USER_TEST_LOGIN = "test";

    @NotNull
    public static final String USER_TEST_PASSWORD = "test";

    @NotNull
    public static final String USER_DEFAULT_LOGIN = "user";

    @NotNull
    public static final String USER_DEFAULT_PASSWORD = "user";

    @NotNull
    String USER_DEFAULT_FIRSTNAME = "user";

    @NotNull
    public static final String USER_ADMIN_LOGIN = "admin";

    @NotNull
    public static final String USER_ADMIN_PASSWORD = "admin";

    @NotNull
    public static final String USER_MANAGER_LOGIN = "manager";

    @NotNull
    public static final String USER_MANAGER_PASSWORD = "manager";

    @NotNull
    public static final String BIN_LOCATE = "./data.bin";

    @NotNull
    public static final String BASE64_LOCATE = "./data.base64";

    @NotNull
    public static final String JSON_LOCATE = "./data.json";

    @NotNull
    public static final String XML_LOCATE = "./data.xml";

    @NotNull
    public static final String YAML_LOCATE = "./data.yaml";

}