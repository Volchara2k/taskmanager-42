package ru.renessans.jvschool.volkov.task.manager.endpoint;

import junitparams.JUnitParamsRunner;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.jupiter.api.BeforeAll;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.marker.IntegrationImplementation;

import java.util.UUID;

@RunWith(value = JUnitParamsRunner.class)
@Category(IntegrationImplementation.class)
public final class UserEndpointTest {

    @NotNull
    private static final SessionEndpointService SESSION_ENDPOINT_SERVICE = new SessionEndpointService();

    @NotNull
    private static final SessionEndpoint SESSION_ENDPOINT = SESSION_ENDPOINT_SERVICE.getSessionEndpointPort();


    @NotNull
    private static final AuthenticationEndpointService AUTHENTICATION_ENDPOINT_SERVICE = new AuthenticationEndpointService();

    @NotNull
    private static final AuthenticationEndpoint AUTH_ENDPOINT = AUTHENTICATION_ENDPOINT_SERVICE.getAuthenticationEndpointPort();


    @NotNull
    private static final AdminEndpointService ADMIN_ENDPOINT_SERVICE = new AdminEndpointService();

    @NotNull
    private static final AdminEndpoint ADMIN_ENDPOINT = ADMIN_ENDPOINT_SERVICE.getAdminEndpointPort();


    @NotNull
    private static final UserEndpointService USER_ENDPOINT_SERVICE = new UserEndpointService();

    @NotNull
    private static final UserEndpoint USER_ENDPOINT = USER_ENDPOINT_SERVICE.getUserEndpointPort();

    @BeforeAll
    static void assertMainComponentsBeforeAll() {
        Assert.assertNotNull(SESSION_ENDPOINT_SERVICE);
        Assert.assertNotNull(SESSION_ENDPOINT);
        Assert.assertNotNull(AUTHENTICATION_ENDPOINT_SERVICE);
        Assert.assertNotNull(AUTH_ENDPOINT);
        Assert.assertNotNull(ADMIN_ENDPOINT_SERVICE);
        Assert.assertNotNull(ADMIN_ENDPOINT);
        Assert.assertNotNull(USER_ENDPOINT_SERVICE);
        Assert.assertNotNull(USER_ENDPOINT);
    }

    @Test
    @TestCaseName("Run testGetUser for getUser(session)")
    public void testGetUser() {
        @NotNull final SessionDTO openSessionResponse = SESSION_ENDPOINT.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @Nullable final UserLimitedDTO userResponse = USER_ENDPOINT.getUser(openSessionResponse);
        Assert.assertNotNull(userResponse);
        @Nullable final SessionDTO closeSessionResponse = SESSION_ENDPOINT.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    @TestCaseName("Run testGetUserRole for getUserRole(session)")
    public void testGetUserRole() {
        @NotNull final SessionDTO openSessionResponse = SESSION_ENDPOINT.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @NotNull final UserRole userRoleResponse = USER_ENDPOINT.getUserRole(openSessionResponse);
        Assert.assertNotNull(userRoleResponse);
        Assert.assertEquals(UserRole.ADMIN, userRoleResponse);
        @Nullable final SessionDTO closeSessionResponse = SESSION_ENDPOINT.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    @TestCaseName("Run testEditProfileById for editProfile(session, \"newFirstName\")")
    public void testEditProfile() {
        @NotNull final SessionDTO openSessionResponse = SESSION_ENDPOINT.openSession(
                DemoDataConst.USER_TEST_LOGIN, DemoDataConst.USER_TEST_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @Nullable final UserLimitedDTO editUserResponse = USER_ENDPOINT.editProfile(openSessionResponse, "newFirstName");
        Assert.assertNotNull(editUserResponse);
        Assert.assertEquals(DemoDataConst.USER_TEST_LOGIN, editUserResponse.getLogin());
        Assert.assertEquals("newFirstName", editUserResponse.getFirstName());
        @Nullable final SessionDTO closeSessionResponse = SESSION_ENDPOINT.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    @TestCaseName("Run testEditProfileWithLastName for editProfileWithLastName(session, \"newData\", \"newData\")")
    public void testEditProfileWithLastName() {
        @NotNull final SessionDTO openSessionResponse = SESSION_ENDPOINT.openSession(
                DemoDataConst.USER_DEFAULT_LOGIN, DemoDataConst.USER_DEFAULT_PASSWORD
        );
        Assert.assertNotNull(openSessionResponse);

        @Nullable final UserLimitedDTO editUserResponse = USER_ENDPOINT.editProfileWithLastName(
                openSessionResponse, "newData", "newData"
        );
        Assert.assertNotNull(editUserResponse);
        Assert.assertEquals(DemoDataConst.USER_DEFAULT_LOGIN, editUserResponse.getLogin());
        Assert.assertEquals("newData", editUserResponse.getFirstName());
        Assert.assertEquals("newData", editUserResponse.getLastName());
        @Nullable final SessionDTO closeSessionResponse = SESSION_ENDPOINT.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
    }

    @Test
    @TestCaseName("Run testUpdatePassword for updatePassword(session, \"{2}\")")
    public void testUpdatePassword() {
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserLimitedDTO addUserResponse = AUTH_ENDPOINT.signUpUser(null, login, password);
        Assert.assertNotNull(addUserResponse);
        @NotNull final SessionDTO openSessionResponse = SESSION_ENDPOINT.openSession(login, password);
        Assert.assertNotNull(openSessionResponse);

        @Nullable final UserLimitedDTO updateUserResponse = USER_ENDPOINT.updatePassword(
                openSessionResponse, "newPassword"
        );
        Assert.assertNotNull(updateUserResponse);
        Assert.assertEquals(login, updateUserResponse.getLogin());
        Assert.assertEquals(addUserResponse.getId(), updateUserResponse.getId());
        @Nullable final SessionDTO closeSessionResponse = SESSION_ENDPOINT.closeSession(openSessionResponse);
        Assert.assertNotNull(closeSessionResponse);
        @NotNull final SessionDTO openAdminSessionResponse = SESSION_ENDPOINT.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(openAdminSessionResponse);
        final int deleteUserResponse = ADMIN_ENDPOINT.deleteUserByLogin(openAdminSessionResponse, login);
        Assert.assertEquals(1, deleteUserResponse);
        @Nullable final SessionDTO closeAdminSessionResponse = SESSION_ENDPOINT.closeSession(openAdminSessionResponse);
        Assert.assertNotNull(closeAdminSessionResponse);
    }

}