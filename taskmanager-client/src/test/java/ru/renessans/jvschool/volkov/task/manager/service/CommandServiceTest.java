package ru.renessans.jvschool.volkov.task.manager.service;

import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.renessans.jvschool.volkov.task.manager.api.repository.ICommandRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICommandService;
import ru.renessans.jvschool.volkov.task.manager.marker.IntegrationImplementation;
import ru.renessans.jvschool.volkov.task.manager.repository.CommandRepository;

import java.util.Collection;
import java.util.Collections;

@Category(IntegrationImplementation.class)
public final class CommandServiceTest {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository(Collections.emptyList());

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @Test
    @TestCaseName("Run testGetAllCommands for getAllCommands()")
    public void testGetAllCommands() {
        Assert.assertNotNull(this.commandService);
        @Nullable final Collection<String> allCommands = this.commandService.getAllCommands();
        Assert.assertNotNull(allCommands);
        Assert.assertNotEquals(0, allCommands.size());
    }

    @Test
    @TestCaseName("Run testGetAllTerminalCommands for getAllTerminalCommands()")
    public void testGetAllTerminalCommands() {
        Assert.assertNotNull(this.commandService);
        @Nullable final Collection<String> allTerminalCommands = this.commandService.getAllTerminalCommands();
        Assert.assertNotNull(allTerminalCommands);
        Assert.assertNotEquals(0, allTerminalCommands.size());
    }

    @Test
    @TestCaseName("Run testAllArgumentCommands for getAllArgumentCommands()")
    public void testAllArgumentCommands() {
        Assert.assertNotNull(this.commandService);
        @Nullable final Collection<String> allArgumentCommands = this.commandService.getAllArgumentCommands();
        Assert.assertNotNull(allArgumentCommands);
        Assert.assertNotEquals(0, allArgumentCommands.size());
    }

}