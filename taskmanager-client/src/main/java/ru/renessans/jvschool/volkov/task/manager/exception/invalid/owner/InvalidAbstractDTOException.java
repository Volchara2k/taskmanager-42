package ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class InvalidAbstractDTOException extends AbstractException {

    @NotNull
    private static final String EMPTY_OWNER =
            "Ошибка! Параметр \"сущность\" отсутствует!\n";

    public InvalidAbstractDTOException() {
        super(EMPTY_OWNER);
    }

}