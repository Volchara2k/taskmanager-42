package ru.renessans.jvschool.volkov.task.manager.api.configuration;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.endpoint.*;

import java.util.concurrent.Executor;

public interface IEndpointConfiguration {

    @NotNull
    Executor executor();

    @NotNull
    AuthenticationEndpointService authenticationEndpointService();

    @NotNull
    AdminEndpointService adminEndpointService();

    @NotNull
    AdminDataInterChangeEndpointService adminDataInterChangeEndpointService();

    @NotNull
    SessionEndpointService sessionEndpointService();

    @NotNull
    UserEndpointService userEndpointService();

    @NotNull
    ProjectEndpointService projectEndpointService();

    @NotNull
    TaskEndpointService taskEndpointService();

    @NotNull
    AuthenticationEndpoint authenticationEndpoint(
            @NotNull AuthenticationEndpointService authenticationEndpointService
    );

    @NotNull
    AdminEndpoint adminEndpoint(
            @NotNull AdminEndpointService adminEndpointService
    );

    @NotNull
    AdminDataInterChangeEndpoint adminDataInterChangeEndpoint(
            @NotNull AdminDataInterChangeEndpointService adminDataInterChangeEndpointService
    );

    @NotNull
    SessionEndpoint sessionEndpoint(
            @NotNull SessionEndpointService sessionEndpointService
    );

    @NotNull
    UserEndpoint userEndpoint(
            @NotNull UserEndpointService userEndpointService
    );

    @NotNull
    ProjectEndpoint projectEndpoint(
            @NotNull ProjectEndpointService projectEndpointService
    );

    @NotNull
    TaskEndpoint taskEndpoint(
            @NotNull TaskEndpointService taskEndpointService
    );

}