package ru.renessans.jvschool.volkov.task.manager.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public final class RuntimeUtil {

    int getAvailableProcessors() {
        return Runtime.getRuntime().availableProcessors();
    }

    long getFreeMemory() {
        return Runtime.getRuntime().freeMemory();
    }

    long getTotalMemory() {
        return Runtime.getRuntime().totalMemory();
    }

    long getMaxMemory() {
        return Runtime.getRuntime().maxMemory();
    }

}