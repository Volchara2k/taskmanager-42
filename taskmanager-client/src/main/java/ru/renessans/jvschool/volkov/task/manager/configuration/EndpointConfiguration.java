package ru.renessans.jvschool.volkov.task.manager.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import ru.renessans.jvschool.volkov.task.manager.api.configuration.IEndpointConfiguration;
import ru.renessans.jvschool.volkov.task.manager.endpoint.*;

import java.util.concurrent.Executor;

@EnableAsync
@Configuration
@ComponentScan("ru.renessans.jvschool.volkov.task.manager")
public class EndpointConfiguration implements IEndpointConfiguration {

    @Bean
    @NotNull
    @Override
    public Executor executor() {
        @NotNull final ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setCorePoolSize(2);
        taskExecutor.setMaxPoolSize(2);
        taskExecutor.setQueueCapacity(500);
        taskExecutor.setThreadNamePrefix("Listener thread-");
        taskExecutor.initialize();
        return taskExecutor;
    }

    @Bean
    @NotNull
    @Override
    public AuthenticationEndpointService authenticationEndpointService() {
        return new AuthenticationEndpointService();
    }

    @Bean
    @NotNull
    @Override
    public AdminEndpointService adminEndpointService() {
        return new AdminEndpointService();
    }

    @Bean
    @NotNull
    @Override
    public AdminDataInterChangeEndpointService adminDataInterChangeEndpointService() {
        return new AdminDataInterChangeEndpointService();
    }

    @Bean
    @NotNull
    @Override
    public SessionEndpointService sessionEndpointService() {
        return new SessionEndpointService();
    }

    @Bean
    @NotNull
    @Override
    public UserEndpointService userEndpointService() {
        return new UserEndpointService();
    }

    @Bean
    @NotNull
    @Override
    public ProjectEndpointService projectEndpointService() {
        return new ProjectEndpointService();
    }

    @Bean
    @NotNull
    @Override
    public TaskEndpointService taskEndpointService() {
        return new TaskEndpointService();
    }

    @Bean
    @NotNull
    @Override
    public AuthenticationEndpoint authenticationEndpoint(
            @NotNull final AuthenticationEndpointService authenticationEndpointService
    ) {
        return authenticationEndpointService.getAuthenticationEndpointPort();
    }

    @Bean
    @NotNull
    @Override
    public AdminEndpoint adminEndpoint(
            @NotNull final AdminEndpointService adminEndpointService
    ) {
        return adminEndpointService.getAdminEndpointPort();
    }

    @Bean
    @NotNull
    @Override
    public AdminDataInterChangeEndpoint adminDataInterChangeEndpoint(
            @NotNull final AdminDataInterChangeEndpointService adminDataInterChangeEndpointService
    ) {
        return adminDataInterChangeEndpointService.getAdminDataInterChangeEndpointPort();
    }

    @Bean
    @NotNull
    @Override
    public SessionEndpoint sessionEndpoint(
            @NotNull final SessionEndpointService sessionEndpointService
    ) {
        return sessionEndpointService.getSessionEndpointPort();
    }

    @Bean
    @NotNull
    @Override
    public UserEndpoint userEndpoint(
            @NotNull final UserEndpointService userEndpointService
    ) {
        return userEndpointService.getUserEndpointPort();
    }

    @Bean
    @NotNull
    @Override
    public ProjectEndpoint projectEndpoint(
            @NotNull final ProjectEndpointService projectEndpointService
    ) {
        return projectEndpointService.getProjectEndpointPort();
    }

    @Bean
    @NotNull
    @Override
    public TaskEndpoint taskEndpoint(
            @NotNull final TaskEndpointService taskEndpointService
    ) {
        return taskEndpointService.getTaskEndpointPort();
    }

}