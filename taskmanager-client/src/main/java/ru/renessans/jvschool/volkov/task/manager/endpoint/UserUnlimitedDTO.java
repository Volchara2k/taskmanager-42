package ru.renessans.jvschool.volkov.task.manager.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for userUnlimitedDTO complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="userUnlimitedDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://endpoint.manager.task.volkov.jvschool.renessans.ru/}userLimitedDTO"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="lockdown" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="passwordHash" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="role" type="{http://endpoint.manager.task.volkov.jvschool.renessans.ru/}userRole" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "userUnlimitedDTO", propOrder = {
        "lockdown",
        "passwordHash",
        "role"
})
public class UserUnlimitedDTO
        extends UserLimitedDTO {

    protected Boolean lockdown;
    protected String passwordHash;
    @XmlSchemaType(name = "string")
    protected UserRole role;

    /**
     * Gets the value of the lockdown property.
     *
     * @return possible object is
     * {@link Boolean }
     */
    public Boolean isLockdown() {
        return lockdown;
    }

    /**
     * Sets the value of the lockdown property.
     *
     * @param value allowed object is
     *              {@link Boolean }
     */
    public void setLockdown(Boolean value) {
        this.lockdown = value;
    }

    /**
     * Gets the value of the passwordHash property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPasswordHash() {
        return passwordHash;
    }

    /**
     * Sets the value of the passwordHash property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPasswordHash(String value) {
        this.passwordHash = value;
    }

    /**
     * Gets the value of the role property.
     *
     * @return possible object is
     * {@link UserRole }
     */
    public UserRole getRole() {
        return role;
    }

    /**
     * Sets the value of the role property.
     *
     * @param value allowed object is
     *              {@link UserRole }
     */
    public void setRole(UserRole value) {
        this.role = value;
    }

    @Override
    public String toString() {
        @NotNull final StringBuilder result = new StringBuilder();
        result.append("Логин: ").append(super.getLogin());
        if (!ValidRuleUtil.isNullOrEmpty(super.getFirstName()))
            result.append(", имя: ").append(super.getFirstName()).append("\n");
        if (!ValidRuleUtil.isNullOrEmpty(super.getLastName()))
            result.append(", фамилия: ").append(super.getLastName()).append("\n");
        result.append("\nРоль: ").append(getRole()).append("\n");
        result.append("\nИдентификатор: ").append(super.getId()).append("\n");
        return result.toString();
    }

}